'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;
const MAX_INT = 2147483647;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Main 
app.get('/', (req, res) => { 
    if (approval(parseInt(req.query.amount))) { 
        res.status(400).end(req.query.amount + ' requires approval.');
    } else {
        res.status(200).end(req.query.amount + ' does not require approval.');
    }
});

// Transaction approval
// If an amount is less than a threashold
// approval is not required
var approval = (value) => {
    var threashold = 1000;
    var surcharge = 10;
    if (parseInt(value) < 0 - surcharge || parseInt(value) > MAX_INT - surcharge) {
      throw new RangeError();
    }
    var amount = Int32Array.from([value],x=>parseInt(x+surcharge));
    //var amount = parseInt(value) + surcharge;
    console.log(amount[0]);
    if (amount[0] >= threashold) {
        return true;
    };
    return false;
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, approval };
